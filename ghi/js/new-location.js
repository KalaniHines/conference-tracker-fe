window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/states/";
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById("state");
        //for each state in states data
        for (let state of data.states) {
            //set option var to create an element at "option" tag
            let option = document.createElement("option");
            console.log(state);
            //set the value of option var to the state abbreviation from
            //api_list_states in state_list ln.67
            option.value = state.abbreviation;
            //set .innerHTML prop of the option element to the state name
            option.innerHTML = state.name;
            //append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        //console.log("formTag: ", event.target);
        //create a form data object from the from element
        const formData = new FormData(formTag);
        //turn it into json using Object.fromEntries()
        const json = JSON.stringify(Object.fromEntries(formData));
        //console.log(json);
        //the FOrmData object needs name attr on the form inputs, name="name" >new-loc.html
        const locationUrl = "http://localhost:8000/api/locations/";
        //console.log(locationUrl);
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": "application/json",
            },
        };
        //console.log(fetchConfig);
        const response = await fetch(locationUrl, fetchConfig);
        //console.log(response);
        //broken here
        if (response.ok) {
            //this will clear the form
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }

    });
    }});
