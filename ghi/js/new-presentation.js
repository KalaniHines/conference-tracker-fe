window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences";
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('conference');
        //console.log(data);
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            //changed id to href
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
            //console.log(option);

        }
    }


    const formTag = document.getElementById('create-presentation-form');
    //no longer need below, its built in later
    //const selectTag = document.getElementById('conference');
    formTag.addEventListener('submit', async (e) => {
        e.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        //Here is the fix
        const conferenceId = document.getElementById("conference").selectedOptions[0].value
        //selectTag.options[selectTag.selectedIndex].value; << that didnt work
        console.log(conferenceId);
        // set the var inside the url
        const locationUrl = `http://localhost:8000${conferenceId}presentations/`;
        //http://localhost:8000/api/conferences/1/presentations/
        //console.log(locationUrl);
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            //console.log(newConference);
        }
    });
});
