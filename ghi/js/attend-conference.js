window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // add the 'd-none' class to the 'loading-conference-spinner'
      document.getElementById('loading-conference-spinner').classList.add('d-none');

      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');

    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async (e) => {
      e.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      //console.log(json);
      //or is it ?
      //const confId = selectTag.value;
      //<<<No, its the same problem route as presentations! Below is the fix
      const conferenceId = document.getElementById('conference').selectedOptions[0].value;
      //took some messin with it but the path for attendeeUrl is below
      //console.log(conferenceId);
      const attendeeUrl = `http://localhost:8001${conferenceId}attendees/`;
      const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
    };
      const attendeeResponse = await fetch(attendeeUrl, fetchConfig);
      if (attendeeResponse.ok) {
        formTag.reset();
        formTag.classList.add('d-none');
        const successAlert = document.getElementById('success-message');
        successAlert.classList.remove('d-none');
      }
      });
    }

  });
