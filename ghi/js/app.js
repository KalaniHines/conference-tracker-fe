function createCard(name, description, pictureUrl, starts, ends, location) {
    //can get the details for each conference, pass it into this
    //function, get a string back, and add it to the innerHTML
    //of the columns
    return `

      <div class="row">
        <div class="col">
        <div class="card shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
             <div class="card-footer">${starts} - ${ends}</div>
        </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    //set a url const to the url of the page at conf.
    const url = 'http://localhost:8000/api/conferences/';
    try {
        //set resp const to await the fetch of the url
       const response = await fetch(url);

       if (!response.ok) {
        throw new Error(response.statusText);
       } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              //console.log(details);
              const title = details.conference.name;
              const location = details.conference.location.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              //inst start date set to details.conference.starts
              const start_date = new Date(details.conference.starts);
              const starts = `${start_date.getMonth()+1}/${start_date.getDate()}/${start_date.getFullYear()}`;

              const end_date = new Date(details.conference.ends);
              const ends = `${end_date.getMonth()+1}/${end_date.getDate()}/${end_date.getFullYear()}`;

              const html = createCard(title, description, pictureUrl, starts, ends, location);
            //   //const column = document.querySelector('.container');
            //   document.querySelector('.col').innerHTML += html;
            //   //column.innerHtml += html;
            //   console.log(html); <<<comment out 62-66
            //below is super cool!
              const column = document.createElement('div');
              column.classList.add('col-md-4', 'mb-4');
              column.innerHTML = html;

              document.querySelector('.row').appendChild(column);
            }
          }

       }
    } catch (e) {
        const bodyTag = document.querySelector('body');
        const error = createError();
        bodyTag.innerHTML += error;

        console.log(e);

        //alert(e);
    }
});

//Ill create a second error function
//although I kinda like the tab that pops up from ln. 67

function createError() {
    return `
    <div class="alert alert-danger" role="alert">
    ERMERGHERD ITSH BROHKIN! Heather, Colin and Farzan wuz here...
    </div>
    `;
}
