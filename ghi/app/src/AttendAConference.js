import React,  {useEffect, useState} from 'react';


function AttendeeForm() {

  const [conference, setConference] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')


  const [conferences, setConferences] = useState([])

  const handleNameChange = event => {
    const value = event.target.value
    setName(value)
  }

  const handleEmailChange = event => {
    const value = event.target.value
    setEmail(value)
  }

  const handleConferenceChange = event => {
    const value = event.target.value
    setConference(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    // create an empty JSON object
    const data = {};
    data.name= name
    data.email = email
    data.conference = conference
    console.log(conference);
    const confId = data.conference;


    //const attendeeUrl = `http://localhost:8001/api/conferences/`;
    //had a 500 server error. But, I had this problem before.
    //the Url needs the var set in it to get the path to post
    const attendeeUrl = `http://localhost:8001${confId}attendees/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      const newAttendee = await response.json();
      //console.log(newAttendee);
      setName('')
      setConference('')
      setEmail('')

    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/ ';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences)

    }
  }
    useEffect(() => {
        fetchData();
      }, []);

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (conferences.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" alt="logo" src="/logo.svg" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className={spinnerClasses} id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleConferenceChange} name="conference" id="conference" className={dropdownClasses} required value={conference}>
                    <option key="default" value="">Choose a conference</option>
                    {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>
                          {conference.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" value={name} />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" value={email} />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AttendeeForm








    // return (
    //     <div classNameName="my-5">
    //     <div classNameName="row">
    //         <div classNameName="col col-sm-auto">
    //         <img width="300" classNameName="bg-white rounded shadow d-block mx-auto mb-4" src="./images/logo.svg" />
    //         </div>
    //         <div classNameName="col">
    //         <div classNameName="card shadow">
    //             <div classNameName="card-body">
    //             <form id="create-attendee-form">
    //                 <h1 classNameName="card-title">It's Conference Time!</h1>
    //                 <p classNameName="mb-3">
    //                 Please choose which conference
    //                 you'd like to attend.
    //                 </p>
    //                 <div classNameName="d-flex justify-content-center mb-3" id="loading-conference-spinner">
    //                 <div classNameName="spinner-grow text-secondary" role="status">
    //                     <span classNameName="visually-hidden">Loading...</span>
    //                 </div>
    //                 </div>
    //                 <div classNameName="mb-3">
    //                 <select name="conference" id="conference" classNameName="form-select d-none" required>
    //                     <option value="">Choose a conference</option>
    //                 </select>
    //                 </div>
    //                 <p classNameName="mb-3">
    //                 Now, tell us about yourself.
    //                 </p>
    //                 <div classNameName="row">
    //                 <div classNameName="col">
    //                     <div classNameName="form-floating mb-3">
    //                     <input required placeholder="Your full name" type="text" id="name" name="name" classNameName="form-control" />
    //                     <label htmlFor="name">Your full name</label>
    //                     </div>
    //                 </div>
    //                 <div classNameName="col">
    //                     <div classNameName="form-floating mb-3">
    //                     <input required placeholder="Your email address" type="email" id="email" name="email" classNameName="form-control" />
    //                     <label htmlFor="email">Your email address</label>
    //                     </div>
    //                 </div>
    //                 </div>
    //                 <button classNameName="btn btn-lg btn-primary">I'm going!</button>
    //             </form>
    //             <div classNameName="alert alert-success d-none mb-0" id="success-message">
    //                 All signed up! See you there!
    //             </div>
    //             </div>
    //         </div>
    //         </div>
    //     </div>
    //     </div>
    // )
