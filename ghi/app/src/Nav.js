import React from 'react';
import { NavLink } from "react-router-dom";

function Nav() {
    return (
        <div>
        {/* Number of attendees: {props.attendees.length}
        ill change this to use the table with the data */}
        <div className="container"></div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
            <a className="navbar-brand" href="/">Conference Tracker!</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                    <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
                    </li>
                    {/* <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/location/new" >New Location</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" href="new-conference.html">New Conference</NavLink>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" aria-current="page" href="new-presentation.html">New presentation</a>
                    </li> */}
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="locations/new" id="new_location">New location</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="conferences/new" id="new_conference">New conference</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="presentations/new" id="new_presentation">New presentation</NavLink>
                    </li>
                    <li className='nav-item'>
                    <NavLink className="btn btn-primary" aria-current="page" to="/attendees/new">Attend!</NavLink>
                    </li>

                </ul>
            </div>
            </div>
            </nav>
            </div>
    );
}


export default Nav;
