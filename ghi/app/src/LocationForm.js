import React, { useEffect, useState } from 'react';


//I pulled the code here from ghi/new-location.html
//changed class=>className and for=>htmlFor
function LocationForm (props) {
    //store name in useState hook compon. state with init default as ''
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');

    const [states, setStates] = useState([]);

    //console.log(states);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.room_count = roomCount;
        data.city = city;
        data.state = state;
        //console.log(data.name);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoomCount('');
            setCity('');
            setState('');

        }
    }



    //event param is the event that triggered the function
    const handleNameChange = (event) => {
        //or:
        const value = event.target.value; //and
        setName(value);
        //target is the html tag that caused the event: user input on the form
        //setName(event.target.value);
    }
    const handleRoomChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }
    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        //console.log("hello");
        const response = await fetch(url);
        //console.log("response test  ");

        if (response.ok) {
            const data = await response.json();
            //console.log(data);
            setStates(data.states);

        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleRoomChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" value={roomCount}/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" value={city}/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
              {/* not sure the handleChange works here yet */}
                <select onChange={handleStateChange} required name="state" id="state" className="form-select" value={state}>
                  {/* removed the selected option before value. to use default and remove error */}
                  <option value="">Choose a state</option>
                  {states.map(state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                        </option>
                    );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default LocationForm;
