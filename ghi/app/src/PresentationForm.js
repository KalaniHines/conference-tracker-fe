import React, { useEffect, useState } from 'react';

function PresentationForm() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const [conferences, setConferences] = useState([]);

    const handleNameChange = (e) => {
        setName(e.target.value);
    }
    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    }
    const handleCompanyNameChange = (e) => {
        setCompanyName(e.target.value);
    }
    const handleTitleChange = (e) => {
        setTitle(e.target.value);
    }
    const handleSynopsisChange = (e) => {
        setSynopsis(e.target.value);
    }
    const handleConferenceChange = (e) => {
        setConference(e.target.value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;
        const confId = data.conference;


        const presentationUrl = `http://localhost:8000${confId}presentations/`;
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');

        }
    }

    const fetchData = async () => {
        const url = `http://localhost:8000/api/conferences/`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }


    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input  onChange={handleNameChange} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control" value={name}/>
                <label htmlFor="name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange} placeholder="Presenter Email" required type="email" name="presenter_email" id="presenter_email" className="form-control" value={email}/>
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company Name" type="text" name="company_name" id="company_name" className="form-control" value={companyName}/>
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={title}/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleSynopsisChange} placeholder="Synopsis" required type="textarea" name="synopsis" id="synopsis" className="form-control" value={synopsis}/>
                <label htmlFor="synopsis">Synopsis</label>
              </div>
              <div className="mb-3">
                {/* <select required id="conference" name="conference" className="form-select">
                  <option select value="">Choose a Conference</option> */}
                  <select onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>
                          {conference.name}
                        </option>
                      );
                    })}

                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
}


export default PresentationForm;
