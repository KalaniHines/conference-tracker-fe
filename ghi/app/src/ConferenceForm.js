import React, { useEffect, useState } from "react";

function ConferenceForm(props) {

    const [name, setName] = useState("");
    const [starts, setStarts] = useState("");
    const [ends, setEnds] = useState("");
    const [description, setDescription] = useState("");
    const [maxPresentations, setMaxPresentations] = useState("");
    const [maxAttendees, setMaxAttendees] = useState("");
    const [location, setLocation] = useState("");

    const [locations, setLocations] = useState([]);




    const handleNameChange = (e) => {
        setName(e.target.value);
    }
    const handleStartsChange = (e) => {
        setStarts(e.target.value);
    }
    const handleEndsChange = (e) => {
        setEnds(e.target.value);
    }
    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    }
    const handleMaxPresentationsChange = (e) => {
        setMaxPresentations(e.target.value);
    }
    const handleMaxAttendeesChange = (e) => {
        setMaxAttendees(e.target.value);
    }
    const handleLocationChange = (e) => {
        setLocation(e.target.value);
    }


    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        //was trying to remember where I needed to set the snake
        //just debugged and found it!
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        //console.log("this is location data: ", location);

        const locationUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            //console.log(newConference);

            //does this work in place of setName('')?
            //NOPE!! Id need to do this to each... No point
            //props.setConference(newConference);
            setName("");
            setStarts("");
            setEnds("");
            setDescription("");
            setMaxPresentations("");
            setMaxAttendees("");
            setLocation("");


        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
            //console.log(state);

        }

    }

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartsChange} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control" value={starts}/>
                    <label htmlFor="starts">Start Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEndsChange} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control" value={ends}/>
                    <label htmlFor="ends">End Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control" value={description}/>
                    <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxPresentationsChange} placeholder="Max Presentations" required type="text" name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations}/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxAttendeesChange} placeholder="Max Attendees" required type="text" name="max_attendees" id="max_attendees" className="form-control" value={maxAttendees}/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    {/* heres where im at<<< */}
                    <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={location}>
                    <option key="default" value="">Choose Location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>{location.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default ConferenceForm;
