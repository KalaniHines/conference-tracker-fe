from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .acls import get_photo, get_weather_data
from .models import Conference, Location, State


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        #added this in after the first bit of JS
        #I used the encoder to add this so I
        #didnt need to update the model!
        "picture_url",
        "id",
        ]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

@require_http_methods(["GET"])
def api_list_states(request):
    if request.method == "GET":
        #set state var to all objects of state ordered by name
        #! took out objects.all().order_by('name')
        states = State.objects.order_by("name")
        #create state_list var set to an empty list
        state_list = []
        #for each state in the database
        for state in states:
            #create a dict that contains the state name and the state abbreviation
            state_list.append(
                {"name": state.name,
                "abbreviation": state.abbreviation,
                }
            )
        #return a json list of all states
        return JsonResponse(
            {"states": state_list}
        )



@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


# def api_show_conference(request, pk):
#     conference = Conference.objects.get(id=pk)
#     weather = get_weather_data(
#         conference.location.city,
#         conference.location.state.abbreviation,
#     )
#     return JsonResponse(
#         {"conference": conference, "weather": weather},
#         encoder=ConferenceDetailEncoder,
#         safe=False,
#     )
@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, pk):
    if request.method == "GET":

        #get the single conf instance by id
        conference = Conference.objects.get(id=pk)
        #*weather data assigned to a var route: city and state
        weather = get_weather_data(
            conference.location.city,
            conference.location.state.abbreviation,
            )
        #return a dict with the conference's data
        #and the weather data
        #?the keys are in the Conf model
        return JsonResponse(
            {"weather": weather, "conference": conference},
            #conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"error": "Conference does not exist"},
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)
        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=pk).update(**content)
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
