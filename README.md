# Your Go-to Conference Tracker!
Built and maintained by Kalani Hines

## I have done step by step work in different dev branches
1. Main is where I merge each dev branch once its stable
2. pre-RESTful is my initial work branch.
3. Encoders is the branch that I added all of the encoders in my views
4. RESTful is the branch that I added the http requirements for Get, Post, Put and Delete
5. Docker is the branch that I added to get my project out of the built in .venv and move to Docker.
   This is the beginning of the project going from Monolithic to MicroService
6. weather branch is where I added the anti corruption layers for Pexels and Weather API keys. I used 3rd party data to assign pictures and weather data to my show_conference and list_locations API's.
7. micro is my branch where I will unmonolith my project. I will also start to take out my pseudocode and docsstrings to clean it up. The pseudo will still be in my dev brances up to weather.

### I finished work in Conference Tracker:
https://gitlab.com/KalaniHines/conference-tracker
### Then started in this repository.

8. My first branch in this new project is FE (Front End). I will implement the beginning of my front end here. I created forms and a login for a user
most of the work was in GHI folder

9. Next branch is react. I created simple forms using DOM manipulation then moved into using hooks and state. I also changed the name of the GHI/Attendees to just app. This will be the whole app and not just the micro service. At the end of this dev branch I had made react forms for attending a conference, creating a new conference, new conf location and new presentation. Next branch will be SPA and that should be the end of this project

10. Next to last branch is SPA. I will make this a single page application and use react routing.

11. Last branch is router. I made react forms (ConferenceForm.js, LocationForm.js, and PresentationForm.js) finished up with the main page and got through the console errors and post issues. It all works now! As soon as I have time Ill get back in and clean up the console logs and merge all into main!


docker-compose up
docker-compose build

http://localhost:3000
